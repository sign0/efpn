module "configure-repository-for-deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = var.repositories
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.base-domain
}

resource "gitlab_project_variable" "api_url" {
  project           = var.frontend_project_id
  key               = "HELM_ENV_VAR_API_URL"
  value             = "https://api.${var.base-domain}"
  protected         = false
  environment_scope = var.gitlab_environment_scope
}

resource "gitlab_project_variable" "basic-auth-user" {
  project           = var.frontend_project_id
  key               = "HELM_ENV_VAR_API_BASIC_AUTH_LOGIN"
  value             = local.auth-user
  protected         = false
  environment_scope = var.gitlab_environment_scope
}

resource "gitlab_project_variable" "basic-auth-password" {
  project           = var.frontend_project_id
  key               = "HELM_ENV_VAR_API_BASIC_AUTH_PASSWORD"
  value             = random_password.password.result
  protected         = false
  environment_scope = var.gitlab_environment_scope
}
