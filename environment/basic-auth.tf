resource "random_password" "password" {
  length = 9
  lifecycle {
    ignore_changes = all
  }
}

locals {
  auth-user = "osiris"
}
module "basic-secret" {
  source    = "gitlab.com/vigigloo/tf-modules-k8s/authsecret"
  version   = "0.0.6"

  secret_name = "basic-app-auth"
  namespace = module.namespace.namespace
  users = {
    osiris = random_password.password.result
  }
}
