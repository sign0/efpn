module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.7"

  chart_name = "postgresql"
  namespace  = module.namespace.namespace
}

resource "gitlab_project_variable" "db-host" {
  project           = var.backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_PG_HOSTNAME"
  value = module.postgresql.host
}

resource "gitlab_project_variable" "db-database" {
  project           = var.backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_PG_DATABASE_OSIRIS"
  value = module.postgresql.dbname
}

resource "gitlab_project_variable" "db-user" {
  project           = var.backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_PG_USERNAME_OSIRIS"
  value = module.postgresql.user
}

resource "gitlab_project_variable" "db-password" {
  project           = var.backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_PG_PASSWORD_OSIRIS"
  value = module.postgresql.password
}

resource "gitlab_project_variable" "db-typeorm" {
  project           = var.backend_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_TYPEORM_URL"
  value = module.postgresql.uri
}
