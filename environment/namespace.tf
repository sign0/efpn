module "namespace" {
  source       = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version      = "0.1.0"
  namespace    = var.namespace
  max_cpu      = var.namespace_quota_max_cpu
  max_memory   = var.namespace_quota_max_memory
  project_name = var.project_name
  project_slug = var.project_slug
}
