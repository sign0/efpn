module "gitlab-runner" {
  source  = "gitlab.com/vigigloo/tf-modules/k8sgitlabrunner"
  version = "0.1.0"

  kubeconfig    = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  project_slug  = var.project_slug
  project_name  = var.project_name
  gitlab_groups = [data.gitlab_group.efpn.id]
}
