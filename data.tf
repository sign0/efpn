data "scaleway_k8s_cluster" "prod" {
  cluster_id = var.prod_cluster_id
}

data "scaleway_k8s_cluster" "dev" {
  cluster_id = var.dev_cluster_id
}

data "gitlab_group" "efpn" {
  group_id = 14415095
}

data "gitlab_project" "backend" {
  id = 32075816
}

data "gitlab_project" "frontend" {
  id = 33737217
}
