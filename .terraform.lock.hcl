# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.8.0"
  constraints = "~> 3.8.0"
  hashes = [
    "h1:ESZehCeHY3G5tCrL47HH+0W18Mt/IYslwaadGfpucU8=",
    "h1:FYcoMEYFpzimWWl1W/PNaVnhc1jazKMXXW/4HncfS1w=",
    "h1:MOSn/Gg9RF+wkUVy9gzPiQb2YBFDy8BNJCMSihZAafU=",
    "h1:Y0fXCEvONCa7ckmt7zFwdaa7KjtN3SnWRNHUdrpd4gI=",
    "zh:02d977e6803ff336054194e567fcd11d095846346a604017afef98ada0353bc1",
    "zh:07294f6061db86c1ed44e5eae01576d76c585128e334761feb01fca5b866d53f",
    "zh:1cfa4cbbdb9a8a235bb82e7f3968982993750fc5a95bd948b9dfd6de772bff0a",
    "zh:3f9172206c00de224e3e234503681c29f918e2b83dbf5b11c0e791694600bec6",
    "zh:71174040a680be48f65b63fb5571bdd76cd17de830c88b81b6e684b56d77616c",
    "zh:81b1c86b8b59c4f3faed59d461555ce53d86b12f9fc91a2afaee89ca3992f154",
    "zh:a09d2012b4392c01a310713c170db1d7e211190b0e2a27e43ec2f7ca59b6d6c9",
    "zh:a97ed255d3d161313370fc34e83018216dcf198e7a5b285abe31b26bf8b048f3",
    "zh:abfad232ec29ecea620e087d20f1586d0b5b4fc02a6369a6b0c65e80b8de3566",
    "zh:c8814ae34933a082bf68db958ed6ef708321093ce9901a11e2efceaa5b3ed031",
    "zh:e5a52eaada2797fbec6aca19bc7a74cdb40dc1625df999f6a93c34690ea2bc80",
    "zh:f4e1787f9fcab92824dad41486eaa6895a789c2d25f3d3f3126d134bd5edfe90",
    "zh:f90b196e6ff444fe721603765a5be5778bc92d45458009bc77ee6d1b6c32bf67",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version = "2.4.1"
  hashes = [
    "h1:CLb4n9f/hLyqqq0zbc+h5SuNOB7KnO65qOOb+ohwsKA=",
    "h1:Gqwrr+yKWR79esN39X9eRCddxMNapmaGMynLfjrUJJo=",
    "h1:aFvUq5HOEwFV/3e7DGU45zDf6j2SThDRjaCAeY2Qfss=",
    "h1:faJiQGaYrNCwcghsysIGMDwJnr0odIx7U8TlF4bxNNY=",
    "zh:07517b24ea2ce4a1d3be3b88c3efc7fb452cd97aea8fac93ca37a08a8ec06e14",
    "zh:11ef6118ed03a1b40ff66adfe21b8707ece0568dae1347ddfbcff8452c0655d5",
    "zh:1ae07e9cc6b088a6a68421642c05e2fa7d00ed03e9401e78c258cf22a239f526",
    "zh:1c5b4cd44033a0d7bf7546df930c55aa41db27b70b3bca6d145faf9b9a2da772",
    "zh:256413132110ddcb0c3ea17c7b01123ad2d5b70565848a77c5ccc22a3f32b0dd",
    "zh:4ab46fd9aadddef26604382bc9b49100586647e63ef6384e0c0c3f010ff2f66e",
    "zh:5a35d23a9f08c36fceda3cef7ce2c7dc5eca32e5f36494de695e09a5007122f0",
    "zh:8e9823a1e5b985b63fe283b755a821e5011a58112447d42fb969c7258ed57ed3",
    "zh:8f79722eba9bf77d341edf48a1fd51a52d93ec31d9cac9ba8498a3a061ea4a7f",
    "zh:b2ea782848b10a343f586ba8ee0cf4d7ff65aa2d4b144eea5bbd8f9801b54c67",
    "zh:e72d1ccf8a75d8e8456c6bb4d843fd4deb0e962ad8f167fa84cf17f12c12304e",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.7.1"
  hashes = [
    "h1:/zifejk3MfLSDQr5J6sc3EHrnFwAVEDH9LrewWMRqe4=",
    "h1:4LPAvfuRY3Kw9bycdOttVqxYryg1DThYhAr9D8jZ+fo=",
    "h1:Df9MZxqgXueXVObeAiPPDQ5aLwQ2bJ2r1gul/IYSxeg=",
    "h1:ylrzJ6MUV3TyFt3gVIShyqFlnszRbv3qwbchR1Gfm8I=",
    "zh:0da320fd81ece6696f7cceda35e459ee97cae8955088af38fc7f2feab1dce924",
    "zh:37d304b8b992518c9c12e8f10437b9d4a0cc5a823c9421ac794ad2347c4d1122",
    "zh:3d4e12fb9588c3b2e782d392fea758c6982e5d653154bec951e949155bcbc169",
    "zh:6bb32b8d5cccf3e3ae7c124ed27df76dc7653ca760c132addeee15272630c930",
    "zh:94775153b90e285876fc17261e8f5338a1ff732f4133336cc68754acb74570b6",
    "zh:a665d1336765cdf8620a8797fd4e7e3cecf789e96e59ba80634336a4390df377",
    "zh:aa8b35e9958cb89f01c115e8866a07d5468fb53f1c227d673e94f7ee8fb76242",
    "zh:b7a571336387d773a74ed6eefa3843ff78d3662f2745c99c95008002a1341662",
    "zh:c50d661782175d50ea4952fe943b0e4a3e33c27aa69e5ff21b3cbfa513e90d0a",
    "zh:e0999b349cc772c75876adbc2a13b5dc256d3ecd7e4aa91baee5fdfcecaa7465",
    "zh:e1399aec06a7aa98e9b0f64b4281697247f338a8a40b79f5f6ebfd43bf4ce1e2",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version = "3.1.2"
  hashes = [
    "h1:JF+aiOtS0G0ffbBdk1qfj7IrT39y/GZh/yl2IhqcIVM=",
    "zh:0daceba867b330d3f8e2c5dc895c4291845a78f31955ce1b91ab2c4d1cd1c10b",
    "zh:104050099efd30a630741f788f9576b19998e7a09347decbec3da0b21d64ba2d",
    "zh:173f4ef3fdf0c7e2564a3db0fac560e9f5afdf6afd0b75d6646af6576b122b16",
    "zh:41d50f975e535f968b3f37170fb07937c15b76d85ba947d0ce5e5ff9530eda65",
    "zh:51a5038867e5e60757ed7f513dd6a973068241190d158a81d1b69296efb9cb8d",
    "zh:6432a568e97a5a36cc8aebca5a7e9c879a55d3bc71d0da1ab849ad905f41c0be",
    "zh:6bac6501394b87138a5e17c9f3a41e46ff7833ad0ba2a96197bb7787e95b641c",
    "zh:6c0a7f5faacda644b022e7718e53f5868187435be6d000786d1ca05aa6683a25",
    "zh:74c89de3fa6ef3027efe08f8473c2baeb41b4c6cee250ba7aeb5b64e8c79800d",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b29eabbf0a5298f0e95a1df214c7cfe06ea9bcf362c63b3ad2f72d85da7d4685",
    "zh:e891458c7a61e5b964e09616f1a4f87d0471feae1ec04cc51776e7dec1a3abce",
  ]
}

provider "registry.terraform.io/hashicorp/time" {
  version = "0.7.2"
  hashes = [
    "h1:/OoDbNb/z86XjvSlgna568gttFfAk9SoWRa277b3Pgw=",
    "h1:0jgplGsGPZceMrYoG1AxxSebBlfwPYT4MkJTewDIy7s=",
    "h1:NKy1QrNLlP5mKy5Tea6lQSRsVoyydJQKh6WvNTdBF4I=",
    "h1:YYLAfhMFP5nhV2iZPslqsLkZN+6sZo7gMJW7pLcLfM8=",
    "zh:0bbe0158c2a9e3f5be911b7e94477586110c51746bb13d102054f22754565bda",
    "zh:3250af7fd49b8aaf2ccc895588af05197d886e38b727e3ba33bcbb8cc96ad34d",
    "zh:35e4de0437f4fa9c1ad69aaf8136413be2369ea607d78e04bb68dc66a6a520b8",
    "zh:369756417a6272e79cad31eb2c82c202f6a4b6e4204a893f656644ba9e149fa2",
    "zh:390370f1179d89b33c3a0731691e772d5450a7d59fc66671ec625e201db74aa2",
    "zh:3d12ac905259d225c685bc42e5507ed0fbdaa5a09c30dce7c1932d908df857f7",
    "zh:75f63e5e1c68e6c5bccba4568c3564e2774eb3a7a19189eb8e2b6e0d58c8f8cc",
    "zh:7c22a2078a608e3e0278c4cbc9c483909062ebd1843bddaf8f176346c6d378b1",
    "zh:7cfb3c02f78f0060d59c757c4726ab45a962ce4a9cf4833beca704a1020785bd",
    "zh:a0325917f47c28a2ed088dedcea0d9520d91b264e63cc667fe4336ac993c0c11",
    "zh:c181551d4c0a40b52e236f1755cc340aeca0fb5dcfd08b3b1c393a7667d2f327",
  ]
}

provider "registry.terraform.io/scaleway/scaleway" {
  version     = "2.2.0"
  constraints = "~> 2.2.0"
  hashes = [
    "h1:0ofLpRZGsTpVlwcrGGRbqkRv2s2nrn5EMOj130vU5bA=",
    "h1:4M58uMZHO9fYFPBug868UjaDo+EutwtEGSsCHi4E3UM=",
    "h1:bybacvEndgogbx/+JlC5e05NspsFncEtD514LZXDfZI=",
    "h1:zBzdxk8KmV8uzhp32ZXJiPzxvzZymmbwS9gAWNIc7rg=",
    "zh:016486d3e448630e29595412b8d31c8a3b2cec83fa531a86ef3d3858f6ebe45d",
    "zh:0ae1eb5142f866da8849475d976da21a2f7cc905f37c99e7aef810c3369e0f65",
    "zh:0c2c8bd7b8beb49d6320cbb4dfba647a465dc1d0f3583b01ad7f0b0559cc043a",
    "zh:1a2ddae054c2e21db960d25432406f01ac0aca7529838176c11a892f62987a8f",
    "zh:211c92dd31513b06d679f1d9d85ed39911ee32a5c03a2af93e7a5710cb6d0a64",
    "zh:76ae419f8ebdc39236c14f87b6c03c9b1f2c6f60081fec48d7d3531cadafcdbe",
    "zh:8a951e6fb2c329d0095edb607e5c760c47838956d7b0a75111692bd77158a445",
    "zh:9ff0638bd03a39aacf331912ff1547d31c5a5a3e1961bf3ca50a581ae2bf0cba",
    "zh:a3ece235aefa0a3110ecae34304122fe32a566a9d54123593e9434693b03d764",
    "zh:a47f101c77be03df35179474110ab05397693ce7efb74c5a133ae3cb0041bb88",
    "zh:a69562aaf71b7ef7b2c8ef345637a3309762f45d960560453595ea2ec2cb0e2c",
    "zh:aa4b8699e888b08905271a6a2c5da206ac737742b8a947a1d9ed53c875954cd5",
    "zh:abb3e3e235b4862ead53397dd09be40c91910931a993b9415185ea6e8492fdb4",
    "zh:e772b6ccb6d70a2d610c696185d3411548c73ad6e10870f70921021134ef1e67",
  ]
}
