variable "scaleway_organization_id" {
  type = string
}

variable "dev_base-domain" {
  type = string
}

variable "prod_base-domain" {
  type = string
}

variable "prod_cluster_id" {
  type = string
}

variable "dev_cluster_id" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}
