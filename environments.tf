module "reviews" {
  source                   = "./environment"
  gitlab_environment_scope = "*"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base-domain              = var.dev_base-domain
  namespace                = "${var.project_slug}-reviews"
  repositories             = [data.gitlab_project.backend.id, data.gitlab_project.frontend.id]
  project_slug             = var.project_slug
  project_name             = var.project_name

  frontend_project_id     = data.gitlab_project.frontend.id
  backend_project_id      = data.gitlab_project.backend.id
  namespace_quota_max_cpu = 4
  #  namespace_quota_max_memory =
}

module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base-domain              = var.dev_base-domain
  namespace                = "${var.project_slug}-development"
  repositories             = [data.gitlab_project.backend.id, data.gitlab_project.frontend.id]
  project_slug             = var.project_slug
  project_name             = var.project_name

  frontend_project_id     = data.gitlab_project.frontend.id
  backend_project_id      = data.gitlab_project.backend.id
  namespace_quota_max_cpu = 4
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base-domain              = var.prod_base-domain
  namespace                = var.project_slug
  repositories             = [data.gitlab_project.backend.id, data.gitlab_project.frontend.id]
  project_slug             = var.project_slug
  project_name             = var.project_name

  frontend_project_id     = data.gitlab_project.frontend.id
  backend_project_id      = data.gitlab_project.backend.id
  namespace_quota_max_cpu = 4
}
